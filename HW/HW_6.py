# 1. Напишіть функцію, яка приймає два аргументи.
# Якщо обидва аргумени відносяться до числових типів функція пермножує ці аргументи і повертає результат
# Якшо обидва аргументи відносяться до типу стрінг функція обʼєднує їх в один і повертає
# В будь-якому іншому випадку - функція повертає кортеж з двох агрументів


def get_input():
    arg1 = input('Enter the first argument: ')
    arg2 = input('Enter the second argument: ')
    return arg1, arg2

def multiply_or_combine(arg1, arg2):
    if isinstance(arg1, (int, float)) and isinstance(arg2, (int, float)):
        return arg1 * arg2
    elif isinstance(arg1, str) and isinstance(arg2, str):
        return arg1 + arg2
    else:
        return (arg1, arg2)

def process_arguments(arg1, arg2):
    try:
        arg1 = float(arg1)
        arg2 = float(arg2)
        result = multiply_or_combine(arg1, arg2)
        print('Multiplied numbers', arg1, 'and', arg2, 'is', result)
    except ValueError:
        result = multiply_or_combine(arg1, arg2)
        if isinstance(result, tuple):
            print('The arguments are of different types:', result)
        else:
            print('The combination of', arg1, 'and', arg2, 'is', result)

arg1, arg2 = get_input()
process_arguments(arg1, arg2)


# 2. Візьміть код з заняття і доопрацюйте натупним чином:
# користувач має вгадати чизло за певну кількість спроб. користувач на початку програми визначає кількість спроб
# додайте підказки. якщо рвзнися між числами більше 10 - "холодно", від 10 до 5 - "тепло", 1-4 - "гаряче"

# game
    # AI -> number
    # User get number from keyboard
    # User == AI

from random import randint

def get_ai_number():
    number = randint(1, 100)
    print(f'AI: {number}')
    return number

def get_user_number():
    while True:
        try:
            return int(input('Enter the number (int): '))
        except ValueError:
            print('Number please!')

def check_numbers(ai_number, user_number):
    result = ai_number == user_number
    return result

def game_guess_number():
    print('Game begins!')

    num_of_attempts = int(input('Enter the number of attempts: '))
    ai_number = get_ai_number()
    hint = ''

    for attempt in range(num_of_attempts):
        user_number = get_user_number()
        is_game_end = check_numbers(ai_number, user_number)
        if is_game_end:
            print('User win!')
            return
        if attempt == num_of_attempts - 1:
            print(f'Game over! The number was {ai_number}.')
            return
        if abs(ai_number - user_number) > 10:
            hint = 'cold'
        elif abs(ai_number - user_number) > 4:
            hint = 'warm'
        else:
            hint = 'hot'
        print(f'Wrong! The number is {hint}! You have {num_of_attempts - attempt - 1} attempts left.')

game_guess_number()

# The get_ai_number() function generates a random integer between 1 and 100 using the randint() function from the random module. The number is stored in the number variable, and then printed to the console using an f-string. The number variable is then returned by the function.
# The get_user_number() function uses a while loop to repeatedly ask the user to input an integer. If the user inputs a non-integer value, a ValueError is raised, and the error message "Number please!" is printed to the console. If the user inputs an integer value, the function returns the input value.
# The check_numbers() function takes in two arguments, ai_number and user_number, which represent the randomly generated number and the user's input number, respectively. The function then checks if the two numbers are equal, and returns a boolean value (True if the numbers are equal, False otherwise).
# The game_guess_number() function is the main game loop. It starts by printing the message "Game begins!" to the console. It then prompts the user to enter the number of attempts they want to have in the game, which is stored in the num_of_attempts variable.
# The function calls the get_ai_number() function to generate a random number, which is stored in the ai_number variable.
# The hint variable is initialized as an empty string.
# The function uses a for loop to iterate over a range of integers from 0 to num_of_attempts - 1. This allows the user to make up to num_of_attempts attempts to guess the AI's number.
# Within the loop, the function calls the get_user_number() function to prompt the user to input a number. The input value is stored in the user_number variable.
# The function then calls the check_numbers() function to check if the ai_number and user_number are equal. If they are, the function prints the message "User win!", and returns, ending the loop.
# If the loop reaches the last iteration (attempt == num_of_attempts - 1), the function prints a message to inform the user that they have run out of attempts, and reveals the AI's number. The function then returns, ending the loop.
# If neither of the above conditions is met, the function calculates the difference between the ai_number and user_number using the abs() function, and uses an if/elif/else statement to assign a value to the hint variable based on the magnitude of the difference. If the difference is greater than 10, the hint is set to "cold". If the difference is between 5 and 10 (inclusive), the hint is set to "warm". If the difference is between 1 and 4 (inclusive), the hint is set to "hot".
# The function then prints a message to inform the user that their guess was incorrect, and includes the hint value and the number of attempts remaining.
# The loop then continues to the next iteration, allowing the user to make another guess.
