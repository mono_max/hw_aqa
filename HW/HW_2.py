# """Сформуйте стрінг, в якому міститься інформація про певне слово.
# "Word [тут слово] has [тут довжина слова, отримайте з самого сдлва] letters",
# наприклад "Word 'Python' has 6 letters".
# Для отримання слова для аналізу скористайтеся константою або функцією input()."""

string = 'Euphoria'
srting_lenght  = len(string)
result = 'Word {} has {} letters'.format(string, srting_lenght)
print(result)

# """Напишіть программу "Касир в кінотеватрі", яка попросіть користувача ввести свсвій вік
# (можно використати константу або функцію input(), на екран має бути виведено лише одне повідомлення,
# також подумайте над варіантами, коли введені невірні дані).
# a. якщо користувачу менше 7 - вивести повідомлення"Де твої батьки?"
# b. якщо користувачу менше 16 - вивести повідомлення "Це фільм для дорослих!"
# c. якщо користувачу більше 65 - вивести повідомлення"Покажіть пенсійне посвідчення!"
# d. якщо вік користувача містить цифру 7 - вивести повідомлення "Вам сьогодні пощастить!"
# e. у будь-якому іншому випадку - вивести повідомлення "А білетів вже немає!" """

age = input('Enter your age: ')
if not age.isdigit():
    print('Enter a integer between 1 and 120')
else:
    digit_age = int(age)
    if digit_age < 1 or digit_age > 120:
        print('Enter a integer between 1 and 120')
    elif '7' in age:
        print('You will be lucky today!')
    elif digit_age < 7:
        print('Where are your parents?')
    elif digit_age <= 16:
        print('This is a movie for adults!')
    elif digit_age >= 65:
        print('Show me your pension certificate!')
    else:
        print('There are no more tickets!')