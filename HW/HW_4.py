# 1. Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
# Напишіть код, який видалить (не створить новий, а саме видалить!) з нього всі числа, які менше 21 і більше 74.

list_number = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
for num in list_number[:]:
    if num <= 21 or num >= 74:
        list_number.remove(num)
print(list_number)

#comprehension

list_numbers = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
list_numbers = [num for num in list_numbers if 21 <= num <= 74]
print(list_numbers)



# 2 . Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів і цінами:
# { "citos": 47.999, "BB_studio" 42.999, "mo-no": 49.999, "my-main-service": 37.245, "buy-now": 38.324,
# "x-store": 37.166, "the-partner": 38.988, "store-123": 37.720, "roze-tka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких потрапляють в діапазон між мінімальною і максимальною ціною.

prices = {
    "citos": 47.999,
    "BB_studio": 42.999,
    "mo-no": 49.999,
    "my-main-service": 37.245,
    "buy-now": 38.324,
    "x-store": 37.166,
    "the-partner": 38.988,
    "store-123": 37.720,
    "roze-tka": 38.003
}
lower_limit = 38.123
upper_limit = 47.339
matches = []
for store, price in prices.items():
    if lower_limit <= price <= upper_limit:
        matches.append(store)
print("Match:", ", ".join(matches))

