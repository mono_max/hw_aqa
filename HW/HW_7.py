# 1. Напишіть функцію, яка визначає сезон за датою. Функція отримує стрінг у форматі "[день].[місяць]"
# (наприклад "12.01", "30.08", "1.11" і тд) і повинна повернути стрінг з відповідним сезоном,
# до якого відноситься ця дата ("літо", "осінь", "зима", "весна")

def get_season(date_str):
    if '.' not in date_str:
        return 'Invalid input. Please enter a date in the format "DD.MM"'
    day, month = date_str.split('.')
    day = int(day)
    month = int(month)

    if month == 12 or month <= 2:
        season = 'winter'
    elif 3 <= month <= 5:
        season = 'spring'
    elif 6 <= month <= 8:
        season = 'summer'
    elif 9 <= month <= 11:
        season = 'autumn'
    else:
        return "Invalid date"

    if month % 3 == 0 and day >= 21:
        if season == 'winter':
            season = 'spring'
        elif season == 'spring':
            season = 'summer'
        elif season == 'summer':
            season = 'autumn'
        else:
            season = 'winter'

    if day < 1 or day > 31:
        return "Invalid day"

    return season

while True:
    date_str = input("Enter a date in the format 'DD.MM': ")
    if not date_str.replace('.', '').isdigit():
        print("Invalid input. Please enter a date in the format 'DD.MM'")
        continue
    season = get_season(date_str)
    if season == "Invalid date":
        print("Invalid date. Please enter a valid date between 01.01 and 31.12")
        continue
    elif season == "Invalid day":
        print("Invalid day. Please enter a valid day between 01 and 31")
        continue
    print(f"The season for {date_str} is {season}")
    break


# 2. Напишіть функцію "Тупий калькулятор", яка приймає два числових аргументи і строковий, який відповідає за операцію між ними (+ - / *).
# Функція повинна повертати значення відповідної операції (додавання, віднімання, ділення, множення), інші операції не допускаються.
# Якщо функція оримала невірний тип данних для операції (не числа) або неприпустимий (невідомий) тип операції
# вона повинна повернути None і вивести повідомлення "Невірний тип даних" або "Операція не підтримується" відповідно.

def dumb_calculator(num1, num2, operator):
    if not isinstance(num1, (int, float)) or not isinstance(num2, (int, float)):
        print("Invalid data type")
        return None

    if operator == "+":
        return num1 + num2
    elif operator == "-":
        return num1 - num2
    elif operator == "*":
        return num1 * num2
    elif operator == "/":
        if num2 == 0:
            print("Cannot devision by 0")
            return None
        return num1 / num2
    else:
        print("Operation not supported")
        return None

result = dumb_calculator(3, 0, "/")
print(result)

# 2 way from console input

def dumb_calculator():
    num1 = input("Enter the first number: ")
    num2 = input("Enter the second number: ")
    operator = input("Enter the operator (+, -, *, /): ")

    try:
        num1 = float(num1)
        num2 = float(num2)
    except ValueError:
        print("Invalid data type")
        return None

    if operator == "+":
        result = num1 + num2
    elif operator == "-":
        result = num1 - num2
    elif operator == "*":
        result = num1 * num2
    elif operator == "/":
        if num2 == 0:
            print("Invalid data type")
            return None
        result = num1 / num2
    else:
        print("Operation not supported")
        return None

    print("Result: ", result)
    return result
dumb_calculator()