# """ 1. Напишіть цикл, який буде вимагати від користувача ввести слово, в якому є буква "о"
# (враховуються як великі так і маленькі).Цикл не повинен завершитися, якщо користувач ввів слово без букви "о"."""

while True:
    word = input("Enter a word with the letter  'o': ")
    if 'o' in word.lower():
        break


# """2. Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
# Зауважте, що lst1 не є статичним і може формуватися динамічно від запуску до запуску."""

list_one = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum',14.88]
list_two = []
for item in list_one:
    if isinstance(item, str):
        list_two.append(item)
print(list_two)

#comprehension

list_one = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
list_two = [item for item in list_one if isinstance(item, str)]
print(list_two)

# """3. Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на "о" (враховуються як великі так і маленькі)."""

text = input('Write your text: ')
words = text.lower().split()
count = 0
for item in words:
    clean_word = item.strip(".!@#$%^&*()][><_/.=?,\";:'")
    if clean_word.endswith('o'):
        count += 1
print("Number of words ending in 'o':", count)