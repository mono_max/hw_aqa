# 1. url https://dummyjson.com/users (100 сторінок)
# вивести в консоль середній вік чоловіків з Brown волоссям,
# а також сформувати список людей, що проживають в місті Louisville

import requests

response = requests.get('https://dummyjson.com/users')
data = response.json()

men_with_brown_hair = []
louisville_residents = []
total_age = 0
count = 0

for user in data:
    if user['gender'] == 'Male' and user['hair_color'] == 'brown':
        men_with_brown_hair.append(user)

    if user['address']['city'] == 'Louisville':
        louisville_residents.append(user['name'])

    if user in men_with_brown_hair:
        total_age += user['age']
        count += 1

if count > 0:
    avg_age = total_age / count
    print(f"The average age of men with brown hair is {avg_age:.2f}")
else:
    print("No men with brown hair found in the data.")

if len(louisville_residents) > 0:
    print("List of people who live in the city of Louisville:")
    for name in louisville_residents:
        print(name)
else:
    print("No residents found in Louisville.")



