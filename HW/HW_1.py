# 1. Задача : Створіть дві змінні first=10, second=30. Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.
first = 10
second = 30
result1 = first + second
print(result1)
result2 = first - second
print(result2)
result3 = first * second
print(result3)
result4 = first ** second
print(result4)
result5 = first / second
print(result5)
result6 = first // second
print(result6)
result7 = first % second
print(result7)

# 2. Задача: Створіть змінну и по черзі запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1. Виведіть на екран результат кожного порівняння.
compare = first < second , first > second , first == second , first != second
print(compare)
# or
compare1 = first < second
compare2 = first > second
compare3 = first == second
compare4 = first != second
print(compare1 , compare2 , compare3 , compare4)
# or
x = first
y = second
print('x > y is ', x < y)
print('x < y is ', x > y)
print('x == y is ', x == y)
print('x != y is ', x != y)

# 3. Задача: Створіть змінну - резкльтат конкатенації строк "Hello " та "world!".
first_word = 'Hello'
second_word = 'world'
result = (first_word + ' ' + second_word)
print(result)
# or other way
str1 = 'Hello'
str2 = 'world'
print(str1 + ' ' + str1)
