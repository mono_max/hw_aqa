import time
import HW_7

# 1. Напишіть декоратор, який визначає час виконання функції. Заміряйте час іиконання функцій з попереднього ДЗ

def time_it(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Execution time: {end_time - start_time:.6f} seconds")
        return result
    return wrapper
