class LocatorsConstants:
     VERIFY_CHECKBOX = "//input[@type='checkbox']"
     SEARCH_EMAIL_FIELD_XPATH = "//*[@id='Email']"
     SEARCH_PASS_FIELD_XPATH = "//*[@id='Password']"
     SEARCH_BUTTON_XPATH = "//button[@type='submit']"
     FIND_BY_XPATH = "//a[contains(text(),'Logout')]"