from selenium import webdriver
from selenium.webdriver.common.by import By
from locators_constants import LocatorsConstants
import time

def test_login():
    driver = webdriver.Chrome()
    driver.get("https://admin-demo.nopcommerce.com/")
    driver.find_element(By.XPATH, LocatorsConstants.VERIFY_CHECKBOX).click()
    driver.find_element(By.XPATH, LocatorsConstants.SEARCH_EMAIL_FIELD_XPATH).clear()
    driver.find_element(By.XPATH, LocatorsConstants.SEARCH_EMAIL_FIELD_XPATH).send_keys("admin@yourstore.com")
    driver.find_element(By.XPATH, LocatorsConstants.SEARCH_PASS_FIELD_XPATH).clear()
    driver.find_element(By.XPATH, LocatorsConstants.SEARCH_PASS_FIELD_XPATH).send_keys("admin")
    driver.find_element(By.XPATH, LocatorsConstants.SEARCH_BUTTON_XPATH).click()
    logout = driver.find_element(By.XPATH, LocatorsConstants.FIND_BY_XPATH)
    assert logout.is_displayed()